package com.robin.rentsure

import akka.NotUsed
import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging

import scala.concurrent.duration._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.delete
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.directives.PathDirectives.path
import spray.json._

import scala.concurrent.{ Await, ExecutionContext, Future, Promise }
import com.robin.rentsure.UserRegistryActor._
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{ Flow, Keep, Sink, Source }
import akka.util.Timeout
import com.robin.rentsure.google.dialogflow.{ TextResponse, WebhookRequest, WebhookResponse }
import com.robin.rentsure.root.insurance.{ GetQuoteRequest, GetTermQuoteRequest, InsuranceGraph, QuoteResultResponse }

import scala.util.{ Failure, Success, Try }

//#user-routes-class
trait UserRoutes extends JsonSupport {
  //#user-routes-class

  // we leave these abstract, since they will be provided by the App
  implicit def system: ActorSystem
  implicit def materializer: ActorMaterializer
  implicit def executionContext: ExecutionContext

  lazy val log = Logging(system, classOf[UserRoutes])

  // other dependencies that UserRoutes use
  def userRegistryActor: ActorRef

  // Required by the `ask` (?) method below
  implicit lazy val timeout = Timeout(5.seconds) // usually we'd obtain the timeout from the system's configuration

  //#all-routes
  //#users-get-post
  //#users-get-delete   
  lazy val userRoutes: Route =
    pathPrefix("google") {
      concat(
        //#users-get-delete
        pathEnd {
          concat(
            post {
              entity(as[String]) { req =>
                val sink = Sink.head[Try[QuoteResultResponse]]
                val whRequest = req.parseJson.convertTo[WebhookRequest]
                val ent = whRequest match {
                  case WebhookRequest("damagecoverage", _, _, params) if params("damage-coverage") == "true" =>
                    HttpEntity(MediaTypes.`application/json`, TextResponse("Great! YAY! PERFECT! ACED!", Vector("requestdamagecoverageamount")).toJson.toString)
                  //                  case WebhookRequest("damagecoverage", _, _, params) if params("damange-coverage") == "false" =>
                  case _ =>
                    println(whRequest)
                    Try(GetTermQuoteRequest(
                      //                  whRequest.parameters("cover").toInt,
                      //                  whRequest.parameters("basic_income").toInt,
                      //                  whRequest.parameters("education"),
                      //                  whRequest.parameters("smoker").toBoolean,
                      //                  whRequest.parameters("gender"),
                      //                  whRequest.parameters("age").toInt
                      10000000,
                      1000000,
                      "grade_12_matric",
                      smoker = false,
                      "male",
                      23
                    )) match {
                      case Success(request) => {
                        val runnableGraph = Source.single(request).via(Flow.fromGraph(InsuranceGraph())).toMat(sink)(Keep.right)
                        val promise = runnableGraph.run()

                        Await.result(promise, 5.seconds) match {
                          case Success(quote) =>
                            val packageName = quote.packageName
                            val premium = quote.premium
                            //                    val premium = quote.suggestedPremium
                            HttpEntity(MediaTypes.`application/json`, TextResponse(s"You can get $packageName from $premium per month.").toJson.toString)
                          case Failure(e) => HttpEntity(MediaTypes.`application/json`, TextResponse(s"Failed to get your quote: $e").toJson.toString)
                        }

                      }
                      case Failure(f) =>
                        val error = f
                        HttpEntity(MediaTypes.`application/json`, TextResponse(s"Error $error").toJson.toString)
                    }
                }
                complete(HttpResponse(status = StatusCodes.OK, entity = ent))
              }
            }
          )
        }
      )
    }
  pathPrefix("users") {
    concat(
      //#users-get-delete
      pathEnd {
        concat(
          get {
            val users: Future[Users] =
              (userRegistryActor ? GetUsers).mapTo[Users]
            complete(users)
          },
          post {
            entity(as[User]) { user =>
              val userCreated: Future[ActionPerformed] =
                (userRegistryActor ? CreateUser(user)).mapTo[ActionPerformed]
              onSuccess(userCreated) { performed =>
                log.info("Created user [{}]: {}", user.name, performed.description)
                complete((StatusCodes.Created, performed))
              }
            }
          }
        )
      },
      //#users-get-post
      //#users-get-delete
      path(Segment) { name =>
        concat(
          get {
            //#retrieve-user-info
            val maybeUser: Future[Option[User]] =
              (userRegistryActor ? GetUser(name)).mapTo[Option[User]]
            rejectEmptyResponse {
              complete(maybeUser)
            }
            //#retrieve-user-info
          },
          delete {
            //#users-delete-logic
            val userDeleted: Future[ActionPerformed] =
              (userRegistryActor ? DeleteUser(name)).mapTo[ActionPerformed]
            onSuccess(userDeleted) { performed =>
              log.info("Deleted user [{}]: {}", name, performed.description)
              complete((StatusCodes.OK, performed))
            }
            //#users-delete-logic
          }
        )
      }
    )
    //#users-get-delete
  }
  //#all-routes
}
