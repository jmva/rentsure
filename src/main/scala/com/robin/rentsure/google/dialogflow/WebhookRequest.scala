package com.robin.rentsure.google.dialogflow

import spray.json._

import scala.util.{ Success, Try }

/**
 *
 * {
 * "contexts": [
 * string
 * ],
 * "lang": string,
 * "query": string,
 * "sessionId": string,
 * "timezone": string
 * }
 *
 */
case class WebhookRequest(
  name: String,
  id: String,
  sessionId: String,
  parameters: Map[String, String]
)

object WebhookRequest {
  import DefaultJsonProtocol._

  implicit val jsonFormat: JsonFormat[WebhookRequest] = new JsonFormat[WebhookRequest] {
    override def read(json: JsValue): WebhookRequest = {
      json.asJsObject.getFields("id", "sessionId", "result") match {
        case Seq(id: JsString, sessionId: JsString, result: JsObject) =>
          val latest = result.fields("contexts").convertTo[Seq[JsValue]].head.asJsObject
          val params = latest.fields("parameters").convertTo[JsObject].fields
            .map {
              case (k, v) =>
                k -> (Try(v.convertTo[String]) match {
                  case Success(s) => s
                  case _ =>
                    val amt = v.convertTo[JsObject].fields("amount")
                    Try(amt.convertTo[String]) match {
                      case Success(x) => x
                      case _ => amt.convertTo[Int].toString
                    }
                })
            }
          val name = latest.fields("name").convertTo[String]
          WebhookRequest(
            name,
            id.convertTo[String],
            sessionId.convertTo[String],
            params
          )
        case _ => throw DeserializationException("Failed to parse webhook (SAD EMOJI)")
      }
    }

    override def write(obj: WebhookRequest): JsObject = {
      JsObject( /**
            * @todo maybe req this
            */ //        "api_key_id" -> JsString(obj.id),
            //        "api_key_secret" -> JsString(obj.secret)
            )
    }
  }
}
