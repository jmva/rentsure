package com.robin.rentsure.google.dialogflow

import spray.json._

case class TextResponse(speech: String, contextOut: Vector[String] = Vector.empty)
case class WebhookResponse(messages: List[TextResponse])

object TextResponse {
  import DefaultJsonProtocol._

  implicit val jsonFormat: JsonFormat[TextResponse] = new JsonFormat[TextResponse] {
    override def read(json: JsValue): TextResponse = {
      json.asJsObject.getFields("message") match {
        //        case Seq(id: JsString, sessionId: JsString, result: JsObject) => TextResponse(
        //          id.convertTo[String],
        //          sessionId.convertTo[String],
        //          result.fields("parameters").convertTo[JsObject].fields
        //            .map { case (k, v) => k -> v.convertTo[String] }
        //        )
        case _ => throw DeserializationException("Failed to parse webhook (SAD EMOJI)")
      }
    }

    override def write(obj: TextResponse): JsObject = {
      JsObject(
        "displayText" -> JsString(obj.speech),
        "speech" -> JsString(obj.speech),
        "type" -> JsNumber(0),
        "contextOut" -> JsArray(obj.contextOut.map(_.toJson))
      )
    }
  }
}

object WebhookResponse {
  import DefaultJsonProtocol._
  implicit val jsonFormat: JsonFormat[WebhookResponse] = new JsonFormat[WebhookResponse] {
    override def read(json: JsValue): WebhookResponse = {
      json.asJsObject.getFields("message") match {
        //        case Seq(id: JsString, sessionId: JsString, result: JsObject) => TextResponse(
        //          id.convertTo[String],
        //          sessionId.convertTo[String],
        //          result.fields("parameters").convertTo[JsObject].fields
        //            .map { case (k, v) => k -> v.convertTo[String] }
        //        )
        case _ => throw DeserializationException("Failed to parse webhook (SAD EMOJI)")
      }
    }

    override def write(obj: WebhookResponse): JsObject = {
      JsObject(
        "messages" -> JsArray(obj.messages.map(_.toJson).toVector)
      )
    }
  }
}