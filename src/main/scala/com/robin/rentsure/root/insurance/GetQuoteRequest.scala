package com.robin.rentsure.root.insurance

import spray.json._

/**
 * {
 * "type": "root_term",
 * "cover_amount": 10000000,
 * "cover_period": "1_year",
 * "basic_income_per_month": 100000,
 * "education_status": "grade_12_no_matric",
 * "smoker": false,
 * "gender": "male",
 * "age": 23
 * }
 */
sealed trait GetQuoteRequest

case class GetTermQuoteRequest(cover: Int, basicIncome: Int, education: String, smoker: Boolean, gender: String, age: Int)
  extends GetQuoteRequest

object GetTermQuoteRequest {
  import DefaultJsonProtocol._

  implicit val jsonFormat: JsonFormat[GetTermQuoteRequest] = new JsonFormat[GetTermQuoteRequest] {
    override def read(json: JsValue): GetTermQuoteRequest = {
      json.asJsObject.getFields("message") match {
        //        case Seq(id: JsString, sessionId: JsString, result: JsObject) => TextResponse(
        //          id.convertTo[String],
        //          sessionId.convertTo[String],
        //          result.fields("parameters").convertTo[JsObject].fields
        //            .map { case (k, v) => k -> v.convertTo[String] }
        //        )
        case _ => throw DeserializationException("Failed to parse quote (SAD EMOJI)")
      }
    }

    override def write(obj: GetTermQuoteRequest): JsObject = {
      JsObject(
        "type" -> JsString("root_term"),
        "cover_amount" -> JsNumber(obj.cover),
        "cover_period" -> JsString("1_year"),
        "basic_income_per_month" -> JsNumber(obj.basicIncome),
        "education_status" -> JsString(obj.education),
        "smoker" -> JsBoolean(obj.smoker),
        "gender" -> JsString(obj.gender),
        "age" -> JsNumber(obj.age)
      )
    }
  }
}