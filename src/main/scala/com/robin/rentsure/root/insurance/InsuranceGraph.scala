package com.robin.rentsure.root.insurance

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.{ Http, model }
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.ws.{ Message, TextMessage, WebSocketRequest }
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.{ ActorMaterializer, FlowShape, Graph, SourceShape }
import akka.stream.scaladsl._
import akka.util.ByteString

import scala.concurrent.{ ExecutionContext, Future }
import scala.concurrent.duration._
import scala.util.{ Success, Try }

object InsuranceGraph {
  val url = "https://sandbox.root.co.za"
  val pwd = "sandbox_NzcwMmE0YzMtMzA3NC00YjZjLWFjYzEtNDZlNTU1NmJkZWJkLk1QRnVWWk4xSDVpeExqY1VKQy1sdEc0TG9kT2ptdHJo"

  def apply()(implicit system: ActorSystem, materializer: ActorMaterializer, ec: ExecutionContext): Graph[FlowShape[GetQuoteRequest, Try[QuoteResultResponse]], NotUsed] = {
    import spray.json._
    import DefaultJsonProtocol._

    GraphDSL.create() { implicit b =>
      import GraphDSL.Implicits._

      val mapsHttpEntity = b.add(
        Flow[GetQuoteRequest].collect[RequestEntity]({
          case r: GetTermQuoteRequest => println(r.toJson.toString); HttpEntity(ContentTypes.`application/json`, r.toJson.toString)
        })
      )

      val mapsRequest = b.add(
        Flow[RequestEntity].map[HttpRequest]({
          entity =>
            val authorization = headers.Authorization(BasicHttpCredentials(pwd, ""))

            model.HttpRequest(
              method = HttpMethods.POST,
              uri = s"$url/v1/insurance/quotes",
              headers = List(authorization),
              entity = entity
            )
        })
      )

      val mapBSResponse = b.add(
        Flow[HttpResponse].mapAsync(1)({
          response =>
            response match {
              case HttpResponse(StatusCodes.OK, _, entity, _) =>
                entity.toStrict(300.millis).map(_.data.utf8String)
              case _ => Future.failed(new IllegalStateException())
            }
        })
      )

      val mapResponse = b.add(
        Flow[String].map[Try[QuoteResultResponse]]({
          x => Try(x.parseJson.convertTo[QuoteResultResponse])
        })
      )

      val httpSingleFlow = b.add(
        Flow[HttpRequest].mapAsync(1)({ request =>
          Http().singleRequest(request)
        })
      )

      mapsHttpEntity ~> mapsRequest ~> httpSingleFlow ~> mapBSResponse ~> mapResponse

      FlowShape(mapsHttpEntity.in, mapResponse.out)
    }
  }
}
