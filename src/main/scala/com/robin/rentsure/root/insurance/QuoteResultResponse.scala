package com.robin.rentsure.root.insurance

import spray.json._

case class QuoteResultResponse(quotePackageId: String, packageName: String, premium: Int, suggestedPremium: Int)

object QuoteResultResponse {
  import DefaultJsonProtocol._

  implicit val jsonFormat: JsonFormat[QuoteResultResponse] = new JsonFormat[QuoteResultResponse] {
    override def read(json: JsValue): QuoteResultResponse = {
      json.convertTo[Seq[JsValue]].head.asJsObject.getFields("quote_package_id", "package_name", "base_premium", "suggested_premium") match {
        case Seq(packageId: JsString, packageName: JsString, basePremium: JsNumber, suggestedPremium: JsNumber) => QuoteResultResponse(
          packageId.convertTo[String],
          packageName.convertTo[String],
          basePremium.convertTo[Int],
          suggestedPremium.convertTo[Int]
        )
        case _ => throw DeserializationException("Failed to parse quote (SAD EMOJI)")
      }
    }

    override def write(obj: QuoteResultResponse): JsObject = {
      /**
       * @todo maybe req
       */
      JsObject( //        "type" -> JsString("root_term"),
      //        "cover_amount" -> JsNumber(obj.cover),
      //        "cover_period" -> JsNumber("1_year"),
      //        "basic_income_per_month" -> JsNumber(obj.basicIncome),
      //        "education_status" -> JsString(obj.education),
      //        "smoker" -> JsBoolean(obj.smoker),
      //        "gender" -> JsString(obj.gender),
      //        "age" -> JsNumber(obj.age)
      )
    }
  }
}

